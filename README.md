# Biblioteca para interface amigável com microcontroladores

Baseado em https://technical.openmobilealliance.org/OMNA/LwM2M/LwM2MRegistry.html

## Estrutura:


![estrutura](documentation.drawio.svg)

# Protocolo

O protocolo é baseado no protocolo LwM2M. O LwM2M é um protocolo de comunicação entre dispositivos e servidores. O LwM2M é um protocolo baseado em REST, que utiliza o modelo de objetos para representar os recursos de um dispositivo.

## Verbos 

O LwM2M utiliza os verbos HTTP para realizar as operações de leitura, escrita e execução de recursos. Os verbos HTTP utilizados são:

- GET: Realiza a leitura de um recurso.
- PUT: Realiza a escrita de um recurso.
- POST: Realiza a execução de um recurso. (NÃO IMPLEMENTADO)
- DELETE: Realiza a exclusão de um recurso.

## Códigos de resposta

O LwM2M utiliza os códigos de resposta HTTP para informar o status de uma operação. Os códigos de resposta HTTP utilizados são:
- 2XX: Operação realizada com sucesso.
    - 200: Operação realizada com sucesso.
- 4XX: Erro de cliente.
    - 400 Bad Request: Requisição inválida.
    - 404 Not Found: Recurso não encontrado.

## URI

O LwM2M utiliza a URI para identificar os recursos de um dispositivo. A URI é composta por três partes, separadas por uma barra (/). A URI é utilizada para realizar as operações de leitura, escrita e execução de recursos.:
- Objeto: Identifica o objeto do dispositivo.
- Instância: Identifica a instância do objeto.
- Recurso: Identifica o recurso da instância.


## Mensagens

As mensagens são formadas por uma string, contendo o VERBO e a URI. A string é enviada para o servidor, que responde com uma mensagem contendo o código de resposta e o conteúdo da mensagem.

# Objetos

Os objetos são representações de recursos de um dispositivo. Os objetos são definidos no LwM2M Registry. O LwM2M Registry é um repositório de objetos LwM2M. O LwM2M Registry é mantido pela Open Mobile Alliance.

## Objetos mais utilizados **

- Digital Input: 3200: [5500, 5518]
- Digital Output: 3201: [5550]
- Analog Input: 3202: [5600, 5821, 5518]
- Analog Output: 3203: [5650]
- Temperature: 3303: [5700, 5821]
- Humidity: 3304: [5700, 5821]
- Time: 3333: [5506]
- Voltage: 3316: [5700, 5821]
- Pressure: 3323: [5700, 5821]
- Set Point: 3308: [5900, 5701]

** Os recursos estão entre colchetes.

# Recursos

Os recursos são representações de informações de um objeto. Os recursos são definidos no LwM2M Registry. 
Os recursos de um objeto são representados por instâncias. Uma instância é uma representação de um recurso. Uma instância é identificada por um identificador único. O identificador único é um número inteiro. O identificador único é único dentro do objeto.

## Recursos mais utilizados:
- Digital Input State: 5500: RW: Boolean
- Digital Output State: 5550: RW: Boolean
- Analog Input Current Value: 5600: R: Float
- Min Measured Value: 5601: R: Float
- Max Measured Value: 5602: R: Float
- Reset Min and Max Measured Values: 5605: E: None
- Analog Output Current Value: 5650: RW: Float
- Sensor Value: 5700: R: Float
- Current Calibration: 5821: RW: Float
- Current Time: 5506: RW: Time: Unix Time. A signed integer representing the number of seconds since Jan 1st, 1970 in the UTC time zone.
- Timestamp: 5518: R: The timestamp of when the measurement was performed.
- Set Point Value: 5900: RW: Float
- Set Point Value: 5701: R: String