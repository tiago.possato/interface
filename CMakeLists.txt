cmake_minimum_required(VERSION 3.0.0)
project(resource)

# check c++11 support
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

# add the library
add_subdirectory(lib)
list(APPEND ADDITIONAL_LIBS Resource smartObjects)

set(SOURCE_FILES src/main.cpp)

add_executable(resource ${SOURCE_FILES})
target_link_libraries(resource ${ADDITIONAL_LIBS})
