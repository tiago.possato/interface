#pragma once

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "string"

#include "smartObjects.h"

using namespace std;

class Resource {
public:
  uint16_t object;
  uint8_t instance;
  uint16_t id;
  string name;
  Operation operation;
  ResourceType type;
  Resource *next;

  Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId, string _name,
           ResourceType _type, void *(*_getValue)());
  Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId, string _name,
           ResourceType _type, void *(*_getValue)(),
           bool (*_setValue)(void *_v));
  Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId, string _name,
           bool (*_erase)());

  /**
   * @brief Get the value of the resource
   *
   * @return void* The value of the resource
   */
  void *getValue();

  string getValueString();

  /**
   * @brief Alterar o valor do recurso.
   * Utilizada somente em recursos com operação RW ou W
   *
   * @param value
   * @return true se o valor foi alterado
   */
  bool setValue(string);

  /**
   * @brief Resetar recursos.
   * Utilizada somente em recursos com operação E
   * A ação executada por este método varia conforme a aplicação
   *
   */
  bool erase();

  /**
   * @brief Get the URI of the resource
   *
   *@return <object>/<instance>/<resource>
   */
  string getUri();
  // define if the resource is being observed
  bool observed = false;

private:
  bool (*_setValue)(void *);
  void *(*_getValue)();
  bool (*_erase)();
};
