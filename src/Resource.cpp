#include "Resource.h"
#include <string>

#include "smartObjects.h"

using namespace std;

Resource::Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId,
                   string _name, ResourceType _type, void *(*_getValue)()) {
  this->id = _resourceId;
  this->name = _name;
  this->operation = OP_R;
  this->type = _type;
  this->_getValue = _getValue;
  this->object = _object;
  this->instance = _instance;
  this->next = NULL;
  this->_setValue = NULL;
  this->_erase = NULL;
  registerResource(this);
}

Resource::Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId,
                   string _name, ResourceType _type, void *(*_getValue)(),
                   bool (*_setValue)(void *_v)) {
  this->id = _resourceId;
  this->name = _name;
  this->operation = OP_RW;
  this->type = _type;
  this->_getValue = _getValue;
  this->object = _object;
  this->instance = _instance;
  this->next = NULL;
  this->_setValue = _setValue;
  this->_erase = NULL;
  registerResource(this);
}

Resource::Resource(uint16_t _object, uint8_t _instance, uint16_t _resourceId,
                   string _name, bool (*_erase)()) {
  this->id = _resourceId;
  this->name = _name;
  this->operation = OP_E;
  this->type = RT_NONE_T;
  this->object = _object;
  this->instance = _instance;
  this->next = NULL;
  this->_setValue = NULL;
  this->_erase = _erase;
  registerResource(this);
}

bool Resource::setValue(string v) {
  if (this->_setValue == NULL || this->operation == OP_R ||
      this->operation == OP_E) {
    return false;
  }

  switch (this->type) {
  case RT_STRING:
    return this->_setValue((void *)&v);
  case RT_INT8_T:
    int8_t a;
    a = (int8_t)strtol(v.c_str(), NULL, 10);
    return this->_setValue((void *)&a);
  case RT_INT16_T:
    int16_t b;
    b = (int16_t)strtol(v.c_str(), NULL, 10);
    return this->_setValue((void *)&b);
  case RT_INT32_T:
    int32_t c;
    c = (int32_t)strtol(v.c_str(), NULL, 10);
    return this->_setValue((void *)&c);
  case RT_UINT8_T:
    uint8_t d;
    d = (uint8_t)strtoul(v.c_str(), NULL, 10);
    return this->_setValue((void *)&d);
  case RT_UINT16_T:
    uint16_t e;
    e = (uint16_t)strtoul(v.c_str(), NULL, 10);
    return this->_setValue((void *)&e);
  case RT_UINT32_T:
    uint32_t f;
    f = (uint32_t)strtoul(v.c_str(), NULL, 10);
    return this->_setValue((void *)&f);
  case RT_FLOAT:
    float i;
    i = (float)strtof(v.c_str(), NULL);
    return this->_setValue((void *)&i);
  case RT_BOOL:
    bool g;
    g = (bool)strtoul(v.c_str(), NULL, 10);
    return this->_setValue((void *)&g);
  default:
    return false;
  }

  return false;
};

bool Resource::erase() {
  if (this->operation == OP_E) {
    if (this->_erase != NULL) {
      return this->_erase();
    }
  }
  return false;
}

void *Resource::getValue() {
  if (this->operation == OP_R || this->operation == OP_RW) {
    if (this->_getValue != NULL) {
      return this->_getValue();
    }
  }
  return NULL;
}

string Resource::getUri() {
  return to_string(this->object) + "/" + to_string(this->instance) + "/" +
         to_string(this->id);
}

string Resource::getValueString() {
  if (this->operation == OP_E || this->operation == OP_W) {
    return "";
  }
  string s;
  switch (this->type) {
  case RT_STRING:
    s = "\"";
    s.append((*(string *)this->getValue()));
    s.append("\"");
    return s;
  case RT_INT8_T:
  case RT_INT16_T:
  case RT_INT32_T:
    return to_string(*(int *)this->getValue());
  case RT_UINT8_T:
  case RT_UINT16_T:
  case RT_UINT32_T:
    return to_string(*(uint *)this->getValue());
  case RT_FLOAT:
    return to_string(*(float *)this->getValue());
  case RT_BOOL:
    return to_string(*(bool *)this->getValue());
  default:
    return "";
  }
}
