#pragma once
#include "Stream.h"
#include <string>

using namespace std;

class Resource;

enum Operation : uint8_t { OP_R = 0, OP_W, OP_RW, OP_E };

enum ResourceType : uint8_t {
  RT_FLOAT = 0,
  RT_STRING,
  RT_BOOL,
  RT_UINT8_T,
  RT_UINT16_T,
  RT_UINT32_T,
  RT_INT8_T,
  RT_INT16_T,
  RT_INT32_T,
  RT_NONE_T
};

// https://technical.openmobilealliance.org/OMNA/LwM2M/LwM2MRegistry.html
// identificadores de Objetos
typedef enum OBJECTS : uint16_t {
  TEMP_3303 = 3303,     // Temperatura em °C
  HUMID_3304 = 3304,    // Umidade relativa do ar em %
  PRESSURE_3308 = 3308, // Pressão atmosférica em hPa
  VOLT_3316 = 3316,     // tensão da bateria em V
  TIME_3333 = 3333,     // horário atual em segundos
  DIGITAL_3200 = 3200,  // estado de um sensor digital
} Object;

typedef enum RESOURCES_ID : uint16_t {
  RESOURCE_5500 = 5500, // Digital Input State: 5500: RW: Boolean
  RESOURCE_5550 = 5550, // Digital Output State: 5550: RW: Boolean
  RESOURCE_5600 = 5600, // Analog Input Current Value: 5600: R: Float
  RESOURCE_5601 = 5601, // Min Measured Value: 5601: R: Float
  RESOURCE_5602 = 5602, // Max Measured Value: 5602: R: Float
  RESOURCE_5605 = 5605, // Reset Min and Max Measured Values: 5605: E: None
  RESOURCE_5650 = 5650, // Analog Output Current Value: 5650: RW: Float
  RESOURCE_5700 = 5700, // Sensor Value: 5700: R: Float
  RESOURCE_5821 = 5821, // Current Calibration: 5821: RW: Float
  RESOURCE_5506 = 5506, // Current Time: 5506: RW: Time: Unix Time. A signed
                        // integer representing the number of seconds since Jan
                        // 1st, 1970 in the UTC time zone.
  RESOURCE_5518 = 5518, // Timestamp: 5518: R: The timestamp of when the
                        // measurement was performed.
  RESOURCE_5900 = 5900, // Set Point Value: 5900: RW: Float
  RESOURCE_5701 = 5701, // Set Point Value: 5701: R: String
} Resource_id;


// register resource
void registerResource(Resource *r);
// print all resources
void printResources(Stream *stream);

string getOperationName(Operation operation);
string getResourceTypeName(ResourceType type);

void processURI(string uri, Stream *stream);