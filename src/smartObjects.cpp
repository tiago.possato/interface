#include <Arduino.h>
#include <freertos/task.h>

#include "Resource.h"
#include "smartObjects.h"
#include <string>

using namespace std;

// list of resources
Resource *Resources = NULL;

struct observeParams {
  Resource *resource;
  uint32_t interval;
  Stream *stream;
};

// register resource
void registerResource(Resource *r) {
  if (Resources == NULL) {
    Resources = r;
  } else {
    Resource *rn = Resources;
    while (rn->next != NULL) {
      rn = rn->next;
    }
    rn->next = r;
  }
}
// print all resources
void printResources(Stream *stream) {
  Resource *r = Resources;
  while (r != NULL) {
    string p = "200 ";
    p.append(r->getUri());
    p.append("/");
    p.append(r->getValueString());
    stream->printf("%s\n", p.c_str());
    r = r->next;
  }
}

template <class T> void _observe(void *parameters) {
  observeParams *params = (observeParams *)parameters;
  uint32_t interval = params->interval > 100 ? params->interval : 100;
  Resource *resource = params->resource;
  Stream *stream = params->stream;
  uint8_t cycle = 0;
  printf("Iniciando observe em %s com %d milli segundos\n",
         resource->getUri().c_str(), interval);

  T lastValue = 0;
  while (true) {
    // Compare atual value with last value
    if (cycle > 2 || lastValue != *(T *)resource->getValue()) {
      cycle = 0;
      // If different, print new value
      string p = "200 ";
      p.append(resource->getUri());
      p.append("/");
      p.append(resource->getValueString());
      stream->printf("%s\n", p.c_str());
      // Update last value
      lastValue = *(T *)resource->getValue();
    }
    cycle++;
    vTaskDelay(interval / portTICK_PERIOD_MS);
  }
}

/**
 * Start task to observe resource with interval
 */
string observe(Resource *res, string _interval, Stream *stream) {
  if (res->operation == OP_E || res->operation == OP_W) {
    return "";
  }

  if (res->observed) {
    return res->getValueString();
  }
  res->observed = true;

  observeParams parameters;
  parameters.resource = res;
  parameters.interval = (uint32_t)strtoul(_interval.c_str(), NULL, 10);
  parameters.stream = stream;

  switch (res->type) {
  case RT_STRING:
    xTaskCreate(_observe<string>, NULL, configMINIMAL_STACK_SIZE * 3,
                &parameters, tskIDLE_PRIORITY + 3, NULL);
    break;
  case RT_INT8_T:
  case RT_INT16_T:
  case RT_INT32_T:
    xTaskCreate(_observe<int>, NULL, configMINIMAL_STACK_SIZE * 3, &parameters,
                tskIDLE_PRIORITY + 3, NULL);
    break;
  case RT_UINT8_T:
  case RT_UINT16_T:
  case RT_UINT32_T:
    xTaskCreate(_observe<uint>, NULL, configMINIMAL_STACK_SIZE * 3, &parameters,
                tskIDLE_PRIORITY + 3, NULL);
    break;
  case RT_FLOAT:
    xTaskCreate(_observe<float>, NULL, configMINIMAL_STACK_SIZE * 3,
                &parameters, tskIDLE_PRIORITY + 3, NULL);
    break;
  case RT_BOOL:
    xTaskCreate(_observe<bool>, NULL, configMINIMAL_STACK_SIZE * 3, &parameters,
                tskIDLE_PRIORITY + 3, NULL);
    break;
  default:
    return "";
  }
  return res->getValueString();
}

bool stractValues(string uri, string _verb, uint16_t *_object,
                  uint8_t *_instance, uint16_t *_id, string *_value) {
  string delimiter = "/";
  size_t pos = 0;
  string token;

  *_object = 0;
  *_instance = 0;
  *_id = 0;

  pos = uri.find(delimiter);
  // printf("pos: %d\n", pos);
  if (pos != string::npos) {
    token = uri.substr(0, pos);
    // printf("%s\n", token.c_str());
    *_object = strtoul(token.c_str(), NULL, 10);
    uri.erase(0, pos + delimiter.length());
  } else {
    return false;
  }

  pos = uri.find(delimiter);
  // printf("pos: %d\n", pos);
  if (pos != string::npos) {
    token = uri.substr(0, pos);
    // printf("%s\n", token.c_str());
    *_instance = strtoul(token.c_str(), NULL, 10);
    uri.erase(0, pos + delimiter.length());
  } else {
    return false;
  }

  pos = uri.find(delimiter);
  // printf("pos: %d\n", pos);
  // Se encontrou mais uma /, então é um comando PUT e pos != -1
  if (pos != string::npos) {
    token = uri.substr(0, pos);
    // printf("%s\n", token.c_str());
    *_id = strtoul(token.c_str(), NULL, 10);
    uri.erase(0, pos + delimiter.length());
  }
  // Se pos = -1, então é um comando GET ou DELETE e precisa extrair o último
  // valor para o id
  else if (uri.length() > 0) {
    token = uri.substr(0, uri.length());
    // printf("%s\n", token.c_str());
    *_id = strtoul(token.c_str(), NULL, 10);
    uri.erase(0, uri.length() + delimiter.length());
  } else {
    return false;
  }

  if (_verb == "GET" || _verb == "DELETE") {
    return true;
  }

  // Caso for um comando PUT, obrigatoriamente tem que ter um valor
  if (_verb == "PUT") {
    if (uri.length() > 0) {
      *_value = uri.substr(0, uri.length());
      return true;
    }
  }

  // Caso o comando for OBSERVE, verifica se foi informado um intervalo.
  //  Caso contrário, assume o valor padrão de 2 segundos
  if (_verb == "OBSERVE") {
    if (uri.length() > 0) {
      *_value = uri.substr(0, uri.length());
      return true;
    } else {
      *_value = "2000";
      return true;
    }
  }
  return false;
}

bool stractVerb(string *_uri, string *_verb) {
  string delimiter = " ";
  size_t pos = 0;

  *_verb = "";

  pos = _uri->find(delimiter);
  // printf("pos: %d\n", pos);
  if (pos != string::npos) {
    *_verb = _uri->substr(0, pos);
    // printf("%s\n", *_verb.c_str());
    _uri->erase(0, pos + delimiter.length());
    return true;
  } else {
    *_verb = *_uri;
  }

  // Verify if the verb is valid
  if (*_verb != "GET" && *_verb != "PUT" && *_verb != "DELETE" &&
      *_verb != "OBSERVE") {
    return false;
  }
  return true;
}

string getOperationName(Operation operation) {
  switch (operation) {
  case OP_R:
    return "R";
  case OP_W:
    return "W";
  case OP_RW:
    return "RW";
  case OP_E:
    return "E";
  default:
    return "Unknown";
  }
}

string getResourceTypeName(ResourceType type) {
  switch (type) {
  case RT_FLOAT:
    return "float";
  case RT_STRING:
    return "string";
  case RT_BOOL:
    return "bool";
  case RT_UINT8_T:
    return "uint8_t";
  case RT_UINT16_T:
    return "uint16_t";
  case RT_UINT32_T:
    return "uint32_t";
  case RT_INT8_T:
    return "int8_t";
  case RT_INT16_T:
    return "int16_t";
  case RT_INT32_T:
    return "int32_t";
  case RT_NONE_T:
    return "None";
  default:
    return "Unknown";
  }
}

void processURI(string uri, Stream *stream) {
  string verb;
  uint16_t object;
  uint8_t instance;
  uint16_t id;
  string value;

  if (!stractVerb(&uri, &verb)) {
    stream->printf("400 Bad Request\n");
    return;
  }
  // printf("%s %s\n", verb.c_str(), uri.c_str());

  if (!stractValues(uri, verb, &object, &instance, &id, &value)) {
    // Caso especial
    if (verb == "GET") {
      printResources(stream);
      stream->printf("\n");
      return;
    }
    stream->printf("400 Bad Request\n");
    return;
  }
  // Remonta a URI com os valores extraídos
  uri = to_string(object) + "/" + to_string(instance) + "/" + to_string(id);

  Resource *r = Resources;
  while (r != NULL) {
    if (r->getUri() == uri) {
      r->name;
      break;
    }
    r = r->next;
  }
  if (r == NULL) {
    stream->printf("404 Not Found\n");
    return;
  }

  if (verb == "GET") {
    uri.append("/");
    uri.append(r->getValueString());
    stream->printf("200 %s\n", uri.c_str());
    return;
  }

  if (verb == "PUT") {
    // printf("Processing value: %s, length: %ld\n", value.c_str(),
    // value.length());
    if (r->setValue(value)) {
      uri.append("/");
      uri.append(r->getValueString());
      stream->printf("200 %s\n", uri.c_str());
      return;
    }
  }

  if (verb == "DELETE") {
    if (r->erase()) {
      stream->printf("200 %s\n", uri.c_str());
      return;
    }
  }

  if (verb == "OBSERVE") {
    string res = observe(r, value, stream);
    // verify if return is NULL
    if (res.length() > 0) {
      uri.append("/");
      uri.append(res);
      stream->printf("200 %s\n", uri.c_str());
      return;
    }
  }
  stream->printf("400 Bad Request\n");
}
