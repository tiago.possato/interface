#pragma once

#include "../lib/Object/smartObjects.h"
#include "../lib/Object/Resource.h"

float temperature = 25.5;
float calibrateTemperature = 0;
uint32_t lastUpdate = 0;

bool calibrateTemp(void *_value)
{
    calibrateTemperature = *(float *)_value;
    return true;
}

void *getTemperature()
{
    static float t;
    t = temperature + calibrateTemperature;
    return &t;
}

void *getCalibrateTemperature()
{
    return &calibrateTemperature;
}

void *getLastUpdate()
{
    return &lastUpdate;
}

Resource t1(3303, 0, 5700, "Temperature value", RT_FLOAT, getTemperature);
Resource t2(3303, 0, 5821, "Calibrate temperature", RT_FLOAT, getCalibrateTemperature, calibrateTemp);
Resource t3(3303, 0, 5518, "Last update", RT_UINT32_T, getLastUpdate);
