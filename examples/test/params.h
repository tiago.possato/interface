#pragma once
#include "string"

#include "../lib/Object/smartObjects.h"
#include "../lib/Object/Resource.h"

using namespace std;

// Parâmetros
struct
{
    uint16_t minTimeOff;
    uint16_t minTimeOn;
} params;

struct
{
    string minTimeOff = "Min Time Off";
    string minTimeOn = "Min Time On";
} paramsUnits;

bool setminTimeOff(void *_value)
{
    params.minTimeOff = *(uint16_t *)_value;
    printf("minTimeOff: %d\n", params.minTimeOff);
    return true;
}

bool setminTimeOn(void *_value)
{
    params.minTimeOn = *(uint16_t *)_value;
    printf("minTimeOn: %d\n", params.minTimeOn);
    return true;
}

void *getminTimeOff()
{
    return &params.minTimeOff;
}

void *getminTimeOn()
{
    return &params.minTimeOn;
}

void *getminTimeOffUnit()
{
    return &paramsUnits.minTimeOff;
}

void *getminTimeOnUnit()
{
    return &paramsUnits.minTimeOn;
}

Resource p1_0(3308, 0, 5900, "minTimeOff setPoint", RT_UINT16_T, getminTimeOff, setminTimeOff);
Resource p1_1(3308, 0, 5701, "minTimeOff unit", RT_STRING, getminTimeOffUnit);

Resource p2_0(3308, 1, 5900, "minTimeOn setPoint", RT_UINT16_T, getminTimeOn, setminTimeOn);
Resource p2_1(3308, 1, 5701, "minTimeOn unit", RT_STRING, getminTimeOnUnit);