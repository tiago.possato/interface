#pragma once

#include "smartObjects.h"
#include "Resource.h"

// Variável booleana para armazenar o estado do led
bool led = false;

// Função para alterar o estado do led
bool setLed(void *_value)
{
    led = *(bool *)_value;
    if (led)
    {
        printf("Ligando led\n");
    }
    else
    {
        printf("Desligando led\n");
    }
    return true;
}

void *getLed(){
    return &led;
}

Resource l1(3201, 0, 5550, "Led", RT_BOOL, getLed, setLed);