#include <iostream>
#include <string>
#include "../lib/Object/Resource.h"
#include "tempSensor.h"
#include "led.h"
#include "params.h"

using namespace std;

int main()
{
	printf("%s\n", processURI("GET").c_str());
	printf("%s\n", processURI("GET 3303/0/5700").c_str());
	printf("%s\n", processURI("GET 3303/0/5821").c_str());
	printf("%s\n", processURI("PUT 3303/0/5821/2").c_str());
	printf("%s\n", processURI("GET 3303/0/5700").c_str());

	for (string line; getline(cin, line);)
	{
		printf("%s\n", processURI(line).c_str());
	}
	return 0;
}